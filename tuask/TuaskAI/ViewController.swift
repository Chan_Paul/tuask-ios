//
//  ViewController.swift
//  TuaskAI
//
//  Created by chenze on 2016/10/28.
//  Copyright © 2016年 share. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

class LanguageHelper {
    class var sharedInstance : LanguageHelper {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : LanguageHelper? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = LanguageHelper()
        }
        return Static.instance!
    }
    // These are the properties you can store in your singleton
    func StringforKey(key:String)->String{
        let lan:String = NSUserDefaults.standardUserDefaults().objectForKey("language") as! String
        let string:String?
        if lan.hasPrefix("zh"){
            let path = NSBundle.mainBundle().pathForResource("zh-Hant", ofType: "lproj")
            let bundle = NSBundle(path: path!)
            string = bundle?.localizedStringForKey(key, value: nil, table: nil)
        }else{
            let path = NSBundle.mainBundle().pathForResource("en", ofType: "lproj")
            let bundle = NSBundle(path: path!)
            string = bundle?.localizedStringForKey(key, value: nil, table: nil)
        }
        
        return string!
    }
}


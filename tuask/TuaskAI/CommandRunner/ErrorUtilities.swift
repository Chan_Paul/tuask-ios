//
//  MDErrorUtilities.swift
//  app
//
//  Created by Ze Chen on 12/1/16.
//  Copyright © 2016 taog. All rights reserved.
//

import Foundation
import SwiftyJSON

class ErrorUtilities {
    static func errorWithCode(code:NSInteger, message:String) -> NSError {
        let result:NSDictionary = ["code":code,"error":message]
        return self.errorFromResult(result)
    }
    
    static func errorFromResult(result:NSDictionary) -> NSError {
        let errorCode:NSInteger = (result["code"]?.integerValue)!
        
        let errorExplanation = result["error"]
        
        let userInfo = NSMutableDictionary(dictionary: result)
        if errorExplanation != nil {
            userInfo[NSLocalizedDescriptionKey] = errorExplanation
        }
        return NSError(domain: ERROR_DOMAIN, code: errorCode, userInfo: userInfo as [NSObject : AnyObject])
    }
    
    static func dealWithHTTPError(operation:AFHTTPRequestOperation) -> NSError?{
        return ErrorUtilities.dealWithHTTPError(operation, mode: nil)
    }
    
    static func dealWithHTTPError(operation:AFHTTPRequestOperation, mode:String?) -> NSError?{
        var error:NSError?
        switch operation.response!.statusCode {
        case HTTP_AUTH_ERROR:
            if mode == "Login" {
                error = ErrorUtilities.errorWithCode(ERROR_AUTH, message: LanguageHelper.sharedInstance.StringforKey("Wrong_password!"))
            }else{
                error = ErrorUtilities.errorWithCode(ERROR_AUTH, message: LanguageHelper.sharedInstance.StringforKey("Session expired!"))
            }
            
            return error
        case HTTP_SERVER_ERROR:
            error = ErrorUtilities.errorWithCode(ERROR_AUTH, message: LanguageHelper.sharedInstance.StringforKey("Server error!"))
            return error
        default:
            return nil
        }
    }
}

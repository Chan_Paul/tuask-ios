//
//  CommandRunner.swift
//  app
//
//  Created by Ze Chen on 12/1/16.
//  Copyright © 2016 myDNA. All rights reserved.
//

import Foundation
import SwiftyJSON


class CommandRunner: AFHTTPRequestOperationManager {
    
    class var currentRunner : CommandRunner {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : CommandRunner? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = CommandRunner()
        }
        return Static.instance!
    }
    
    func runCommand(command: RESTCommand, block:CommandResponseBlock){
        let task:AFHTTPRequestOperation = self.HTTPRequestOperationWithRequest(self.requestConstructor(command)!, success: { (operation, response) in
            if response is NSDictionary {
                print(response)
                block(operation,JSON(response),nil)
            }else{
                block(operation,JSON([]),nil)
            }
            
            }, failure: { (operation, error) in
                block(operation,nil,error)
            }, autoRetryOf: 1, retryInterval: 30)
        self.operationQueue.addOperation(task)
    }
    
    func requestConstructor(command: RESTCommand) -> NSMutableURLRequest?{
        var request:NSMutableURLRequest = NSMutableURLRequest()
        if command.multipartData != nil {
            request = self.requestSerializer.multipartFormRequestWithMethod(command.HTTPMethod!, URLString: command.HTTPPath!, parameters: command.parameters as? [NSObject : AnyObject],constructingBodyWithBlock: {
                (multipartoFormData:AFMultipartFormData) ->Void in
                multipartoFormData.appendPartWithFileData(command.multipartData!, name: command.fileDataName!, fileName: "ios.png", mimeType: "image/png")
                
                }, error:nil)
        }else{
            request = self.requestSerializer.requestWithMethod(command.HTTPMethod!, URLString: command.HTTPPath!, parameters: command.parameters as? [NSObject : AnyObject], error: nil)
        }
        
        
        if command.header != nil {
            request.addValue(command.header!.objectForKey(HTTP_HEADER_UID) as! String, forHTTPHeaderField: HTTP_HEADER_UID)
            request.addValue(command.header!.objectForKey(HTTP_HEADER_ACCESSTOKEN) as! String, forHTTPHeaderField: HTTP_HEADER_ACCESSTOKEN)
            request.addValue(command.header!.objectForKey(HTTP_HEADER_CLIENT) as! String, forHTTPHeaderField: HTTP_HEADER_CLIENT)
        }
        
        return request
    }
    
    
    
}
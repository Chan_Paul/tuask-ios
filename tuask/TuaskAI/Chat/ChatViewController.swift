//
//  ViewController.swift
//  TableView-Chat
//
//  Created by liujunbin on 16/5/17.
//  Copyright © 2016年 liujunbin. All rights reserved.
//

import UIKit
import SocketRocket
import SwiftyJSON
import URBNAlert

class ChatViewController: ViewController , UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,SRWebSocketDelegate, UIScrollViewDelegate{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContainer1: UIView!
    @IBOutlet weak var scrollViewContainer2: UIView!
    @IBOutlet weak var scrollViewContainer3: UIView!
    
    var refreshControl: UIRefreshControl?
    var refreshing: Bool = false {
        didSet {
            if (self.refreshing) {
                self.refreshControl?.beginRefreshing()
            }
            else {
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    
    var _webSocket:SRWebSocket?
    
    var cover:UIButton!
    
    
    @IBOutlet weak var chatTable: ChatTableView! {
        didSet{
            chatTable.translatesAutoresizingMaskIntoConstraints = false
            chatTable.separatorStyle = .None
            chatTable.backgroundColor = UIColor.ChatBackgroundColor()
            chatTable.estimatedRowHeight = 50
            chatTable.rowHeight = UITableViewAutomaticDimension
            chatTable.rootChatViewController = self
        }
    }
    
    var vd : [String : AnyObject] = [String : AnyObject]()
    
    var testString1:String = "Hello"
    var testString2:String = "Anything else?"
    var width1:CGFloat = 0
    var width2:CGFloat = 0
    var button1:UIButton!
    var button2:UIButton!
    
    @IBOutlet weak var footerContrainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.ChatBackgroundColor()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.layer.zPosition = -50
        self.chatTable.addSubview(refreshControl!)
        self.refreshControl?.addTarget(self, action: #selector(ChatViewController.onPullToFresh), forControlEvents: UIControlEvents.ValueChanged)
        
        
        cover = UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        cover.backgroundColor = UIColor.clearColor()
        cover.hidden = true
        self.view.addSubview(cover!)
        self.view.bringSubviewToFront(cover)
        cover.addTarget(self, action: #selector(ChatViewController.onCoverTouched), forControlEvents: .TouchUpInside)
        
        followInit()
    }
    
    func followInit() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(ChatViewController.respondToSwipeGesture(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ChatViewController.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.scrollView.addGestureRecognizer(swipeLeft)
        self.scrollView.addGestureRecognizer(swipeRight)
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                print("Swiped right")
                scrollToLeft()
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped left")
                scrollToRight()
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func scrollToLeft(){
        if scrollView.contentOffset.x == 0 {
        }else if scrollView.contentOffset.x == scrollView.contentSize.width/3 {
            scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
        }else if scrollView.contentOffset.x == scrollView.contentSize.width*2/3{
            scrollView.setContentOffset(CGPoint(x: scrollView.contentSize.width/3,y: 0), animated: true)
        }
    }
    
    func scrollToRight(){
        if scrollView.contentOffset.x == 0 {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentSize.width/3,y: 0), animated: true)
        }else if scrollView.contentOffset.x == scrollView.contentSize.width/3 {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentSize.width*2/3,y: 0), animated: true)
        }else if scrollView.contentOffset.x == scrollView.contentSize.width*2/3{
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        createSenderButtons()
    }
    
    func createSenderButtons() {
        width1 = testString1.widthWithConstrainedHeight(35, font: UIFont.boldSystemFontOfSize(15)) + 40
        width2 = testString2.widthWithConstrainedHeight(35, font: UIFont.boldSystemFontOfSize(15)) + 40
        let leading = (footerContrainer.frame.width - (width1 + width2 + 10))/2
        button1 = UIButton(frame: CGRectMake(leading, 60, width1, 35))
        button1.setBackgroundImage(UIImage(named: "bubbleRPurple"), forState: .Normal)
        button1.setTitle(testString1, forState: .Normal)
        button1.titleLabel?.font = UIFont.boldSystemFontOfSize(15)
        button1.addTarget(self, action: #selector(ChatViewController.onLeftButtonTouched(_:)), forControlEvents: .TouchUpInside)
        footerContrainer.addSubview(button1)
        
        button2 = UIButton(frame: CGRectMake(leading + width1 + 10, 60, width2, 35))
        button2.setBackgroundImage(UIImage(named: "bubbleRPurple"), forState: .Normal)
        button2.setTitle(testString2, forState: .Normal)
        button2.titleLabel?.font = UIFont.boldSystemFontOfSize(15)
        button2.addTarget(self, action: #selector(ChatViewController.onRightButtonTouched(_:)), forControlEvents: .TouchUpInside)
        footerContrainer.addSubview(button2)
        UIView.animateWithDuration(0.2, delay: 0.3, options: .CurveEaseInOut, animations: {
            self.button1.frame = CGRectMake(leading, 10, self.width1, 35)
        }) { (succeed) in}
        UIView.animateWithDuration(0.2, delay: 0.4, options: .CurveEaseInOut, animations: {
            self.button2.frame = CGRectMake(leading + self.width1 + 10, 10, self.width2, 35)
        }) { (succeed) in}
    }
    
    func onLeftButtonTouched(sender:UIButton) {
        
        var height:CGFloat = 0
        for cell in chatTable.visibleCells {
            height = height + cell.frame.height
        }
        self.addBlank(testString2)
        
        if height > chatTable.frame.height-10 {
            height = chatTable.frame.height-50
        }
        UIView.animateWithDuration(0.8, delay: 1, options: .CurveEaseInOut, animations: {
            self.button1.frame = CGRectMake(self.footerContrainer.frame.width - self.width1 - 13, -(self.view.frame.height - self.footerContrainer.frame.height - height - 30), self.width1, 35)
            self.button1.alpha = 0
        }) { (succeed) in
            self.chatTable.data?.removeLastObject()
            self.chatTable.data?.addObject(Message(message_type: "text", content: self.testString1, attachment: "",isFromSocket: false))
            let cell:BlankChatViewCell = self.chatTable.cellForRowAtIndexPath(NSIndexPath(forRow: self.chatTable.data!.count-1, inSection: 0)) as! BlankChatViewCell
            cell.data = ChatViewData(message:Message(message_type: "text", content: self.testString1, attachment: "",isFromSocket: false) , role: .Sender)
        }
    }
    
    func onRightButtonTouched(sender:UIButton) {
        
        var height:CGFloat = 0
        for cell in chatTable.visibleCells {
            height = height + cell.frame.height
        }
        self.addBlank(testString2)
        
        if height > chatTable.frame.height-10 {
            height = chatTable.frame.height-50
        }
        UIView.animateWithDuration(0.8, delay: 1, options: .CurveEaseInOut, animations: {
            self.button2.frame = CGRectMake(self.footerContrainer.frame.width - self.width2 - 13, -(self.view.frame.height - self.footerContrainer.frame.height - height - 30), self.width2, 35)
            self.button2.alpha = 0
        }) { (succeed) in
            self.chatTable.data?.removeLastObject()
            self.chatTable.data?.addObject(Message(message_type: "text", content: self.testString2, attachment: "",isFromSocket: false))
            let cell:BlankChatViewCell = self.chatTable.cellForRowAtIndexPath(NSIndexPath(forRow: self.chatTable.data!.count-1, inSection: 0)) as! BlankChatViewCell
            cell.data = ChatViewData(message:Message(message_type: "text", content: self.testString2, attachment: "",isFromSocket: false) , role: .Sender)
        }
    }
    
    func onPullToFresh() {
        self.refreshing = true
        if chatTable.data != nil {
            fetchMoreChatMessages(chatTable.data!.count)
        }else{
            fetchChatMessages()
        }
        
    }
    
    func fetchChatMessages() {
        let array = NSMutableArray()
        array.addObject(Message(message_type: "text", content: "An eclectic mix of Lovestruck and Datetix members", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "Hosted at Magnum", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "Complimentary drink included in the ticket price", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "Complimentary drink included in the ticket price", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "Complimentary drink included in the ticket price", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "Complimentary drink included in the ticket price", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "Live DJ", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "A spooky 666 lucky draw", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "A spooky 666 lucky draw", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "A spooky 666 lucky draw", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "A spooky 666 lucky draw", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "A spooky 666 lucky draw", attachment: "",isFromSocket: false))
        array.addObject(Message(message_type: "text", content: "A spooky 666 lucky draw", attachment: "",isFromSocket: false))
        self.chatTable.data = array
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(self.scrollTab), userInfo: nil, repeats: false)
        //        Message.fetchChatMessagesWithBlock(chat_id,offset: 0) { (messages, error) in
//            if error == nil{
//                self.chatTable.data = NSMutableArray(array: messages)
//                self.chatTable.scrollToBottom(false)
//            }
//        }
    }
    
    func fetchMoreChatMessages(offset:Int) {
//        Message.fetchChatMessagesWithBlock(chat_id,offset: offset) { (messages, error) in
//            if error == nil{
//                if let realDatasource:NSMutableArray = NSMutableArray(array: messages) {
//                    let tempDatasource:NSMutableArray = NSMutableArray(array: self.chatTable.data!)
//                    realDatasource.addObjectsFromArray(tempDatasource as [AnyObject])
//                    self.chatTable.data = realDatasource
//                    self.chatTable.scrollToRowAtIndexPath(NSIndexPath(forRow: messages.count, inSection: 0), atScrollPosition: .Top, animated: false)
//                }
//            }
//            self.refreshing = false
//        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.reconnect()
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        delegate.isInChatRoom = true
        
        self.fetchChatMessages()

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        delegate.isInChatRoom = false
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        _webSocket?.close()
        _webSocket = nil
    }
    
    //Socket Handling
    func reconnect() {
        _webSocket?.delegate = nil;
        _webSocket?.close();
        
//        _webSocket = SRWebSocket(URL: NSURL(string: "ws://lazy-staging.herokuapp.com/cable"))
//        _webSocket!.delegate = self;
//        _webSocket!.open()
    }
    
    func webSocketDidOpen(webSocket: SRWebSocket!) {
        print("Websocket Connected")
//        let strChannel = String(format: "{ \"channel\": \"MessagesChannel\", \"chat_id\": \(chat_id.integerValue)}")
//        let parameters = ["command":"subscribe","identifier":strChannel]
//        
//        
//        do {
//            let jsonData = try NSJSONSerialization.dataWithJSONObject(parameters, options: NSJSONWritingOptions.PrettyPrinted)
//            // here "jsonData" is the dictionary encoded in JSON data
//            let datastring = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
//            _webSocket!.send(datastring)
//        } catch let error as NSError {
//            print(error)
//        }
    }
    
    func webSocket(webSocket: SRWebSocket!, didFailWithError error: NSError!) {
        print(":( Websocket Failed With Error")
        print(error)
    }
    
    func webSocket(webSocket: SRWebSocket!, didReceiveMessage message: AnyObject!) {
        print(message)
        if let text = message as? String {
            let result = convertStringToDictionary(text)!
            if let message = result["message"] as? [String:AnyObject] {
                if let json = message["json"] as? [String:AnyObject] {
                    let result = ChatModelUtilities._fetchChatMessageFromJSON(JSON(json))
                    print(json)
                    print(result)
                    
                    var isDuplicate = false
                    if self.chatTable.data != nil {
                        for data in self.chatTable.data! {
                            if let message = data as? Message{
                                if message.message_id == result.message_id {
                                    isDuplicate = true
                                }
                            }
                        }
                    }
                    if !isDuplicate {
                        self.chatTable.data?.addObject(result)
                        self.chatTable.reloadData()
                        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(self.scrollTab), userInfo: nil, repeats: false)
                    }
                    
                    
                }
            }
        }
        
    }
    @IBAction func addTestData(sender: AnyObject) {
        addLoading()
        let triggerTime = (Int64(NSEC_PER_SEC) * 2)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
            self.removeLoading()
            self.chatTable.data?.addObject(Message(message_type: "text", content: "Only $180! It's going to be an unmissable event, so tell your single friends and reserve your ticket online now: ", attachment: "",isFromSocket: true))
            self.chatTable.reloadData()
            NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(self.scrollTab), userInfo: nil, repeats: false)
        })
        
    }
    
    func addLoading() {
        self.chatTable.data?.addObject(Message(message_type: "indicator", content: "", attachment: "",isFromSocket: true))
        self.chatTable.reloadData()
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(self.scrollTab), userInfo: nil, repeats: false)
    }
    
    func removeLoading()  {
        self.chatTable.data?.removeLastObject()
        let indexPath = NSIndexPath(forRow: self.chatTable.data!.count, inSection: 0)
        self.chatTable.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
    }
    
    func addBlank(content:String) {
        self.chatTable.data?.addObject(Message(message_type: "blank", content: content, attachment: "",isFromSocket: true))
        self.chatTable.reloadData()
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(self.scrollTab), userInfo: nil, repeats: false)
    }
    
    func removeBlank()  {
        self.chatTable.data?.removeLastObject()
        let indexPath = NSIndexPath(forRow: self.chatTable.data!.count, inSection: 0)
        self.chatTable.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
    }
    
    func animateTable() {
        
        let cells = chatTable.visibleCells
        let tableHeight: CGFloat = chatTable.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animateWithDuration(1.0, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0);
                }, completion: nil)
            
            index += 1
        }
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func webSocket(webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!, wasClean: Bool) {
        print("WebSocket closed")
    }
    
    func webSocket(webSocket: SRWebSocket!, didReceivePong pongPayload: NSData!) {
        print("WebSocket received pong")
    }
    
    //
    
    
    func onCoverTouched(){
        self.view.endEditing(true)
        removeCover()
    }
    
    func addCover() {
        cover.hidden = false
    }
    
    func removeCover() {
        cover.hidden = true
    }
    
    
    func renderTableView(){
//        if let txt = chatTool.inputTool.text {
//            Message.postTextMessageWithBlock(chat_id, text: txt, block: { (message, error) in
//                self.chatTool.inputTool.text = ""
//            })
            
//        }
        
    }
    
    func renderImageTableView(){
//        chatTool.inputTool.resignFirstResponder()
    }
    
    func postImage(image:UIImage) {
//        Message.postImageMessageWithBlock(chat_id, image: UIImagePNGRepresentation(image)!, block: { (message, error) in
//        })
    }
    
    func scrollTab(){
        chatTable.scrollToBottom(true)
    }
    
}

extension ChatViewController {
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.renderTableView()
        return false
    }
    
    
}




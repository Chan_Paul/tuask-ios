//
//  StringExt.swift
//  app
//
//  Created by Ze Chen on 22/1/16.
//  Copyright © 2016 taog. All rights reserved.
//

import Foundation

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: self.startIndex.advancedBy(r.startIndex), end: self.startIndex.advancedBy(r.endIndex)))
    }
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
    
    static func localizedPrice(number:NSDecimalNumber, local:NSLocale) -> String{
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = local
        formatter.roundingMode = .RoundHalfUp
        formatter.maximumFractionDigits = 0
        return formatter.stringFromNumber(number)!
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat, font:UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    static func customScheduleToString(custom_schedule:NSSet) -> String {
        let stringArray = NSMutableArray()
        let sortedSet = custom_schedule.sort({ (element1, element2) -> Bool in
            return (element1 as! NSNumber).integerValue < (element2 as! NSNumber).integerValue
        })
        for i in sortedSet{
            if let num:NSNumber = i as? NSNumber {
                switch num.integerValue {
                case 0:
                    stringArray.addObject(LanguageHelper.sharedInstance.StringforKey("Sun"))
                case 1:
                    stringArray.addObject(LanguageHelper.sharedInstance.StringforKey("Mon"))
                case 2:
                    stringArray.addObject(LanguageHelper.sharedInstance.StringforKey("Tue"))
                case 3:
                    stringArray.addObject(LanguageHelper.sharedInstance.StringforKey("Wed"))
                case 4:
                    stringArray.addObject(LanguageHelper.sharedInstance.StringforKey("Thu"))
                case 5:
                    stringArray.addObject(LanguageHelper.sharedInstance.StringforKey("Fri"))
                case 6:
                    stringArray.addObject(LanguageHelper.sharedInstance.StringforKey("Sat"))
                    
                default:
                    break
                }
            }
            
        }
        var frequencyString = LanguageHelper.sharedInstance.StringforKey("Every")
        for i in 0 ..< stringArray.count - 1 {
            frequencyString = frequencyString + (stringArray[i] as! String) + ", "
        }
        frequencyString = frequencyString + (stringArray.lastObject as! String)
        
        return frequencyString
    }
}

extension NSDate {
    func numberOfDaysUntilDateTime(toDateTime: NSDate, inTimeZone timeZone: NSTimeZone? = nil) -> Int {
        let calendar = NSCalendar.currentCalendar()
        if let timeZone = timeZone {
            calendar.timeZone = timeZone
        }
        
        var fromDate: NSDate?, toDate: NSDate?
        
        calendar.rangeOfUnit(.Day, startDate: &fromDate, interval: nil, forDate: self)
        calendar.rangeOfUnit(.Day, startDate: &toDate, interval: nil, forDate: toDateTime)
        
        let difference = calendar.components(.Day, fromDate: fromDate!, toDate: toDate!, options: [])
        return difference.day
    }
    
    static func convertStringToDate(string:String) -> NSDate?{
        let dateformatter = NSDateFormatter()
        dateformatter.locale = NSLocale(localeIdentifier: "en_US")
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'.'SSS'Z'"
        let date = dateformatter.dateFromString(string)
        return date
    }
    
    static func convertDateToStringForWeightChart(date:NSDate) -> String!{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "dd/MM"
        let string = dateformatter.stringFromDate(date)
        
        return string
    }
    
    static func convertDateToStringForBirthday(date:NSDate) -> String!{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "dd/MM/yyyy"
        let string = dateformatter.stringFromDate(date)
        
        return string
    }
    
    static func convertDateToStringForExpiration(date:NSDate) -> String!{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "dd MMMM yyyy"
        let lan:String = NSUserDefaults.standardUserDefaults().objectForKey("language") as! String
        if lan.containsString("Hant"){
            dateformatter.dateFormat = "yyyy年MMMMdd日"
            dateformatter.locale = NSLocale(localeIdentifier: "zh_Hant_HK")
        }
        
        let string = dateformatter.stringFromDate(date)
        
        return string
    }
    
    static func convertDateToStringForUpdate(date:NSDate) -> String!{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let string = dateformatter.stringFromDate(date)
        
        return string
    }
    
    static func convertDateToDateStringForUpdate(date:NSDate) -> String!{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let string = dateformatter.stringFromDate(date)
        
        return string
    }
    
    static func convertBirthdateToAge(string:String) -> Int!{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateformatter.dateFromString(string)
        let calendar = NSCalendar.currentCalendar()
        let ageComponents = calendar.components(.Year,
                                                fromDate: date!,
                                                toDate: NSDate(),
                                                options: [])
        let age = ageComponents.year
        
        return age
    }
    
    static func convertDateToCommonString(date:NSDate) -> String!{
        let lan:String = NSUserDefaults.standardUserDefaults().objectForKey("language") as! String
        let dateFormatter = NSDateFormatter()
        if lan.hasPrefix("zh"){
            dateFormatter.dateFormat = "yyyy年MM月dd日 HH時mm分"
        }else{
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        }
        return dateFormatter.stringFromDate(date)
    }
    
    static func convertDateToCommonDateString(date:NSDate) -> String!{
        let lan:String = NSUserDefaults.standardUserDefaults().objectForKey("language") as! String
        let dateFormatter = NSDateFormatter()
        if lan.hasPrefix("zh"){
            dateFormatter.dateFormat = "yyyy年MM月dd日"
        }else{
            dateFormatter.dateFormat = "dd-MM-yyyy"
        }
        return dateFormatter.stringFromDate(date)
    }
    
    static func convertDateToCommonTimeString(date:NSDate) -> String!{
        let lan:String = NSUserDefaults.standardUserDefaults().objectForKey("language") as! String
        let dateFormatter = NSDateFormatter()
        if lan.hasPrefix("zh"){
            dateFormatter.dateFormat = "HH時mm分"
        }else{
            dateFormatter.dateFormat = "HH:mm"
        }
        return dateFormatter.stringFromDate(date)
    }
}

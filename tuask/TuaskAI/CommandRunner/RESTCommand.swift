//
//  TGRESTCommand.swift
//  app
//
//  Created by Ze Chen on 14/1/16.
//  Copyright © 2016 taog. All rights reserved.
//

import Foundation

class RESTCommand: NSObject {
    var HTTPPath:String?
    var HTTPMethod:String?
    var header:NSDictionary?
    var parameters:NSDictionary?
    var multipartData:NSData?
    var fileDataName:String?
}
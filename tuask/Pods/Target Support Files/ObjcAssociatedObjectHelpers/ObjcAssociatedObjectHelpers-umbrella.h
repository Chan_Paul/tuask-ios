#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "ObjcAssociatedObjectHelpers.h"
#import "NSObject+AssociatedDictionary.h"

FOUNDATION_EXPORT double ObjcAssociatedObjectHelpersVersionNumber;
FOUNDATION_EXPORT const unsigned char ObjcAssociatedObjectHelpersVersionString[];

